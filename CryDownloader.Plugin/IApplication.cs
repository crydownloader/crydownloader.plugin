﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Threading;

namespace CryDownloader.Plugin
{
    public interface IApplication
    {
        ILogManager LogManager { get; }

        Dispatcher Dispatcher { get; }

        IWebBrowser CreateBrowser();

        void ResolveCaptcha(IWebBrowser browser, IHtmlNode captchaNode,
            IEnumerable<IHtmlNode> hiddenNodes, Func<bool> checkCallback, Action successCallback);

        void SaveSettings();

        bool Download(Uri url, string path,
            Action startDownload, Action<DownloadCompleteEventArgs> downloadComplete,
            Action<DownloadProgressChangedEventArgs> downloadProgressChanged);

        T LoadJsonObject<T>(string json, IJsonConverter converter = null);
        T LoadJsonObject<T>(TextReader reader, IJsonConverter converter = null);

        IHtmlDocument LoadHtmlDocument(string html);

        IFileState CreateFileState(FileStateIcon icon, string description);
        IFileState GetFileState(FileStateType type);
        IFileState GetFileState(FileDownloadState fileState, FileStateIcon icon, string description);
        IFileState GetFileState(FileDownloadState fileState);

        bool Muxing(DownloadEntry entry, MuxingArgs args, Action startDownload, 
            Action<DownloadCompleteEventArgs> downloadComplete, 
            Action<DownloadProgressChangedEventArgs> downloadProgressChanged);

        IJavaScriptProgram ParseJavaScript(string script);

        string GetContainerName(string filename);

        bool ScanLink(Uri url, IContainer container, CancellationToken? cancellationToken);
        bool ResolveDownloadUrl(Uri url, CancellationToken? cancellationToken, out Uri resolvedUrl);
    }
}
