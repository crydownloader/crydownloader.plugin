﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptArrayInstance : IJavaScriptObjectInstance
    {
        uint GetLength();
    }
}
