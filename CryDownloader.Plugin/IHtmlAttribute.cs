﻿namespace CryDownloader.Plugin
{
    public interface IHtmlAttribute
    {
        string Name { get; }
        string Value { get; }
        int StreamPosition { get; }
        AttributeValueQuote QuoteType { get; }
        IHtmlNode OwnerNode { get; }
        string OriginalName { get; }
        string DeEntitizeValue { get; }
        string XPath { get; }
        int ValueLength { get; }
        int ValueStartIndex { get; }
        int LinePosition { get; }
        int Line { get; }
        bool UseOriginalName { get; }
    }
}
