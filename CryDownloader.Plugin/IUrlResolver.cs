﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public interface IUrlResolver
    {
        Uri ResolveUrl(Uri url, CancellationToken? cancellationToken);
    }
}
