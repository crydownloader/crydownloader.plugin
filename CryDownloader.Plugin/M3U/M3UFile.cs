﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public sealed class M3UFile
    {
        public M3UMetaDataCollection MetaData { get; }
        public string Path { get; }

        internal M3UFile(string path, List<string> attributeList)
        {
            Path = path;
            MetaData = new M3UMetaDataCollection();

            foreach (var attrList in attributeList)
            {
                string[] splt = attrList.Split(':');

                if (splt.Length < 1)
                    throw new InvalidFormatException("Invalid attribute list");

                M3UMetaData metaData = MetaData.AddMetaData(splt[0]);

                if (splt.Length > 1)
                    M3UPlaylist.ParseAttribtues(splt[1], metaData);
            }
        }

        public override string ToString()
        {
            return Path;
        }
    }
}
