﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CryDownloader.Plugin
{
    public sealed class M3UPlaylist
    {
        public M3UMetaDataCollection MetaData { get; }

        public M3UFileCollection Files { get; }

        public M3UPlaylist()
        {
            MetaData = new M3UMetaDataCollection();
            Files = new M3UFileCollection();
        }

        public M3UPlaylist(TextReader reader)
            : this()
        {
            Load(reader);
        }

        public void Load(TextReader reader)
        {
            string line = reader.ReadLine();

            if (line == null)
                throw new InvalidFormatException();

            if (line == "#EXTM3U")
                LoadExtended(reader);
            else
            {
                Files.AddFile(line);
                LoadNormal(reader);
            }
        }

        private void LoadNormal(TextReader reader)
        {
            string line;

            while ((line = reader.ReadLine()) != null)
                Files.AddFile(line);
        }

        private void LoadExtended(TextReader reader)
        {
            string line;

            Dictionary<string, List<string>> files = new Dictionary<string, List<string>>();
            List<string> Attributes = new List<string>();

            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("#"))
                    Attributes.Add(line);
                else
                {
                    files.Add(line, Attributes);
                    Attributes = new List<string>();
                }
            }

            if (files.Count > 1)
                FindPlaylistAttributes(files);

            foreach (var file in files)
                Files.AddFile(new M3UFile(file.Key, file.Value));
        }

        private void FindPlaylistAttributes(Dictionary<string, List<string>> files)
        {
            List<string> file1 = files.ElementAt(0).Value;
            List<string> file2 = files.ElementAt(1).Value;

            if (file1.Count != file2.Count)
            {
                List<Tuple<string, string>> attrs1 = new List<Tuple<string, string>>();
                List<Tuple<string, string>> attrs2 = new List<Tuple<string, string>>();

                GetAttributeNames(file1, attrs1);
                GetAttributeNames(file2, attrs2);

                foreach (var attr in attrs1)
                {
                    bool bFound = false;

                    foreach (var attr2 in attrs2)
                    {
                        if (attr.Item1 == attr2.Item1)
                        {
                            bFound = true;
                            break;
                        }
                    }

                    if (!bFound)
                    {
                        file1.Remove(attr.Item2);

                        string[] splt = attr.Item2.Split(':');

                        if (splt.Length < 1)
                            throw new InvalidFormatException("Invalid attribute list");

                        M3UMetaData metaData = MetaData.AddMetaData(attr.Item1);

                        if (splt.Length > 1)
                            ParseAttribtues(splt[1], metaData);
                    }
                }
            }
        }

        internal static void ParseAttribtues(string str, M3UMetaData metaData)
        {
            bool ignore = false;
            bool bIsName = true;

            string name = string.Empty;
            string value = string.Empty;

            for (int i = 0; i < str.Length; ++i)
            {
                if (str[i] == ',' && !ignore)
                {
                    metaData.Values.AddMetaData(name, value);
                    name = string.Empty;
                    value = string.Empty;
                    bIsName = true;
                }
                else if (str[i] == '=' && !ignore)
                    bIsName = !bIsName;
                else if (str[i] == '"')
                    ignore = !ignore;
                else if (bIsName)
                    name += str[i];
                else
                    value += str[i];
            }

            metaData.Values.AddMetaData(name, value);
        }

        private void GetAttributeNames(List<string> attrList, List<Tuple<string, string>> attrNames)
        {
            foreach (var attr in attrList)
            {
                string[] splt = attr.Split(':');

                if (splt.Length < 1)
                    throw new InvalidFormatException("Invalid attribute list");

                attrNames.Add(new Tuple<string, string>(splt[0], attr));
            }
        }
    }

    public sealed class InvalidFormatException : Exception
    {
        public InvalidFormatException()
        { }

        public InvalidFormatException(string msg)
            : base(msg)
        { }
    }
}
