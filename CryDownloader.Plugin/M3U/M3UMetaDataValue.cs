﻿namespace CryDownloader.Plugin
{
    public sealed class M3UMetaDataValue
    {
        public string Name { get; }
        public string Value { get; }

        public M3UMetaDataValue(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
