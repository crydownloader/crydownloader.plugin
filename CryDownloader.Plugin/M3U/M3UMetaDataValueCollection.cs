﻿using System.Collections;
using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public sealed class M3UMetaDataValueCollection : IDictionary<string, M3UMetaDataValue>
    {
        private readonly Dictionary<string, M3UMetaDataValue> m_values;

        public M3UMetaDataValue this[string key] { get => m_values[key]; set => m_values[key] = value; }

        public ICollection<string> Keys => m_values.Keys;

        public ICollection<M3UMetaDataValue> Values => m_values.Values;

        public int Count => m_values.Count;

        public bool IsReadOnly => true;

        public M3UMetaDataValueCollection()
        {
            m_values = new Dictionary<string, M3UMetaDataValue>();
        }

        internal void AddMetaData(string name, string value)
        {
            m_values.Add(name, new M3UMetaDataValue(name, value));
        }

        void IDictionary<string, M3UMetaDataValue>.Add(string key, M3UMetaDataValue value)
        {
            m_values.Add(key, value);
        }

        void ICollection<KeyValuePair<string, M3UMetaDataValue>>.Add(KeyValuePair<string, M3UMetaDataValue> item)
        {
            ((ICollection<KeyValuePair<string, M3UMetaDataValue>>)m_values).Add(item);
        }

        void ICollection<KeyValuePair<string, M3UMetaDataValue>>.Clear()
        {
            m_values.Clear();
        }

        bool ICollection<KeyValuePair<string, M3UMetaDataValue>>.Contains(KeyValuePair<string, M3UMetaDataValue> item)
        {
            return ((ICollection<KeyValuePair<string, M3UMetaDataValue>>)m_values).Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return m_values.ContainsKey(key);
        }

        void ICollection<KeyValuePair<string, M3UMetaDataValue>>.CopyTo(KeyValuePair<string, M3UMetaDataValue>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<string, M3UMetaDataValue>>)m_values).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<string, M3UMetaDataValue>> GetEnumerator()
        {
            return m_values.GetEnumerator();
        }

        bool IDictionary<string, M3UMetaDataValue>.Remove(string key)
        {
            return m_values.Remove(key);
        }

        bool ICollection<KeyValuePair<string, M3UMetaDataValue>>.Remove(KeyValuePair<string, M3UMetaDataValue> item)
        {
            return ((ICollection<KeyValuePair<string, M3UMetaDataValue>>)m_values).Remove(item);
        }

        public bool TryGetValue(string key, out M3UMetaDataValue value)
        {
            return m_values.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
