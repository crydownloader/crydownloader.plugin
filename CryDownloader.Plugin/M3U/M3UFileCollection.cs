﻿using System.Collections;
using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public sealed class M3UFileCollection : ICollection<M3UFile>
    {
        private readonly List<M3UFile> m_files;

        public int Count => m_files.Count;

        public bool IsReadOnly => true;

        public M3UFileCollection()
        {
            m_files = new List<M3UFile>();
        }

        internal void AddFile(string filePath)
        {
            m_files.Add(new M3UFile(filePath, new List<string>()));
        }

        internal void AddFile(M3UFile file)
        {
            m_files.Add(file);
        }

        void ICollection<M3UFile>.Add(M3UFile item)
        {
            m_files.Add(item);
        }

        void ICollection<M3UFile>.Clear()
        {
            m_files.Clear();
        }

        public bool Contains(M3UFile item)
        {
            return m_files.Contains(item);
        }

        public void CopyTo(M3UFile[] array, int arrayIndex)
        {
            m_files.CopyTo(array, arrayIndex);
        }

        public IEnumerator<M3UFile> GetEnumerator()
        {
            return m_files.GetEnumerator();
        }

        bool ICollection<M3UFile>.Remove(M3UFile item)
        {
            return m_files.Remove(item);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
