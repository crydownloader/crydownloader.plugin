﻿using System.Collections;
using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public sealed class M3UMetaDataCollection : IDictionary<string, M3UMetaData>
    {
        private readonly Dictionary<string, M3UMetaData> m_values;

        public M3UMetaData this[string key] { get => m_values[key]; set => m_values[key] = value; }

        public ICollection<string> Keys => m_values.Keys;

        public ICollection<M3UMetaData> Values => m_values.Values;

        public int Count => m_values.Count;

        public bool IsReadOnly => true;

        public M3UMetaDataCollection()
        {
            m_values = new Dictionary<string, M3UMetaData>();
        }

        internal M3UMetaData AddMetaData(string name)
        {
            var result = new M3UMetaData(name);
            m_values.Add(name, result);
            return result;
        }

        void IDictionary<string, M3UMetaData>.Add(string key, M3UMetaData value)
        {
            m_values.Add(key, value);
        }

        void ICollection<KeyValuePair<string, M3UMetaData>>.Add(KeyValuePair<string, M3UMetaData> item)
        {
            ((ICollection<KeyValuePair<string, M3UMetaData>>)m_values).Add(item);
        }

        void ICollection<KeyValuePair<string, M3UMetaData>>.Clear()
        {
            m_values.Clear();
        }

        bool ICollection<KeyValuePair<string, M3UMetaData>>.Contains(KeyValuePair<string, M3UMetaData> item)
        {
            return ((ICollection<KeyValuePair<string, M3UMetaData>>)m_values).Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return m_values.ContainsKey(key);
        }

        void ICollection<KeyValuePair<string, M3UMetaData>>.CopyTo(KeyValuePair<string, M3UMetaData>[] array, int arrayIndex)
        {
            ((ICollection<KeyValuePair<string, M3UMetaData>>)m_values).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<string, M3UMetaData>> GetEnumerator()
        {
            return m_values.GetEnumerator();
        }

        bool IDictionary<string, M3UMetaData>.Remove(string key)
        {
            return m_values.Remove(key);
        }

        bool ICollection<KeyValuePair<string, M3UMetaData>>.Remove(KeyValuePair<string, M3UMetaData> item)
        {
            return ((ICollection<KeyValuePair<string, M3UMetaData>>)m_values).Remove(item);
        }

        public bool TryGetValue(string key, out M3UMetaData value)
        {
            return m_values.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
