﻿namespace CryDownloader.Plugin
{
    public sealed class M3UMetaData
    {
        public string Name { get; }
        public M3UMetaDataValueCollection Values { get; set; }

        public M3UMetaDataValue this[string key] { get => Values[key]; set => Values[key] = value; }

        public M3UMetaData(string name)
        {
            Name = name;
            Values = new M3UMetaDataValueCollection();
        }
    }
}
