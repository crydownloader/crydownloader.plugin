﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptVariableDeclarator : IJavaScriptExpression
    {
        IJavaScriptIdentifier Id { get; }

        IJavaScriptExpression Init { get; }
    }
}
