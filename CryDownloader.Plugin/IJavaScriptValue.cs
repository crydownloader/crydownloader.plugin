﻿using System;

namespace CryDownloader.Plugin
{
    public interface IJavaScriptValue : IEquatable<IJavaScriptValue>
    {
        bool IsPrimitive();
        bool IsUndefined();
        bool IsArray();
        bool IsDate();
        bool IsRegExp();
        bool IsObject();
        bool IsString();
        bool IsNumber();
        bool IsBoolean();
        bool IsNull();
        object AsObject();
        object AsArray();
        object AsDate();
        object AsRegExp();
        T TryCast<T>(Action<IJavaScriptValue> fail = null) where T : class;
        bool AsBoolean();
        string AsString();
        double AsNumber();
        object ToObject();
        IJavaScriptValue Invoke(params IJavaScriptValue[] arguments);
        IJavaScriptValue Invoke(IJavaScriptValue thisObj, IJavaScriptValue[] arguments);
    }
}
