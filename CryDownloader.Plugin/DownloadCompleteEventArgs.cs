﻿using System;

namespace CryDownloader.Plugin
{
    public sealed class DownloadCompleteEventArgs : EventArgs
    {
        public bool IsCancelled { get; }

        public Exception Error { get; }

        public bool Retry { get; set; }

        public DownloadCompleteEventArgs(bool isCancelled, Exception error)
        {
            IsCancelled = isCancelled;
            Error = error;
            Retry = false;
        }
    }
}
