﻿namespace CryDownloader.Plugin
{
    public enum JavaScriptTokens
    {
        BooleanLiteral = 1,
        EOF,
        Identifier,
        Keyword,
        NullLiteral,
        NumericLiteral,
        Punctuator,
        StringLiteral,
        RegularExpression
    }
}
