﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public interface IPictureSource
    {
        string Name { get; }
        string Host { get; }

        List<Uri> GetPictures(Uri uri, CancellationToken? cancelToken);
    }
}
