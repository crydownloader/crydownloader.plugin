﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows;

namespace CryDownloader.Plugin
{
    public abstract class Hoster : IPlugin
    {
        public const string UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36";
        public abstract string Name { get; }
        public abstract string Host { get; }
        public abstract Bitmap Icon { get; }
        public abstract FrameworkElement SettingControl { get; }
        public ILogManager LogManager => Application.LogManager;
        public IApplication Application { get; }

        public Hoster(IApplication app)
        {
            Application = app;
        }

        public void CreateEntry(Uri url, IContainerCreator creator, CancellationToken? cancellationToken)
        {
            List<Uri> urls = new List<Uri>() { url };
            CreateEntries(urls, creator, null, cancellationToken);
        }

        public void CreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            OnCreateEntries(urls, creator, scanUrlChangedCallback, cancellationToken);
        }

        public virtual void Destroy()
        { }

        public abstract void Initialize();

        public abstract void LoadSettings(Dictionary<string, string> properties);

        public abstract Dictionary<string, string> GetSettings();

        public virtual bool IsValidUrl(Uri url)
        {
            return url.ToString().Contains(Host);
        }

        public M3UPlaylist LoadPlaylist(string text)
        {
            using (var reader = new StringReader(text))
            {
                return new M3UPlaylist(reader);
            }
        }

        protected virtual bool? CheckFileInfo(Uri url)
        {
            try
            {
                FileInfo fi = new FileInfo(url.Segments[url.Segments.Length - 1]);

                if (fi.Extension.Contains("htm") || fi.Extension.Contains("php"))
                    return true;
                else
                {
                    try
                    {
                        System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                        req.Method = "HEAD";
                        (req as HttpWebRequest).ServicePoint.ConnectionLimit = int.MaxValue;

                        using (System.Net.WebResponse resp = req.GetResponse())
                        {
                            string contentType = resp.Headers.Get("Content-Type");

                            if (contentType.Contains("html"))
                                return true;
                        }
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                try
                {
                    System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                    req.Method = "HEAD";

                    using (System.Net.WebResponse resp = req.GetResponse())
                    {
                        string contentType = resp.Headers.Get("Content-Type");

                        if (contentType.Contains("html"))
                            return true;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }

            return false;
        }

        protected virtual void OnCreateEntry(CreateEntryArgs args)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnCreateEntries(IEnumerable<Uri> urls, IContainerCreator creator, Action<Uri> scanUrlChangedCallback, CancellationToken? cancellationToken)
        {
            CreateEntryArgs args = new CreateEntryArgs(creator, cancellationToken);

            if (cancellationToken.HasValue)
            {
                foreach (var url in urls)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return;

                    scanUrlChangedCallback?.Invoke(url);

                    args.Url = url;
                    args.IsHTML = CheckFileInfo(url);
                    creator.CurrentUrl = url;

                    try
                    {
                        OnCreateEntry(args);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                foreach (var url in urls)
                {
                    scanUrlChangedCallback?.Invoke(url);

                    args.Url = url;
                    args.IsHTML = CheckFileInfo(url);
                    creator.CurrentUrl = url;

                    try
                    {
                        OnCreateEntry(args);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
        }

        public string DownloadPage(Uri url, CancellationToken? cancellationToken = null, object userData = null)
        {
            if (cancellationToken != null)
            {
                if (cancellationToken.HasValue)
                {
                    if (cancellationToken.Value.IsCancellationRequested)
                        return string.Empty;
                }
            }

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Timeout = 1000 * 60  * 5;
            request.ServicePoint.ConnectionLimit = int.MaxValue;
            request.UserAgent = UserAgent;
            OnDownloadPage(request, url, userData);

            try
            {
                using (var response = request.GetResponse())
                {
                    using (var stream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            return reader.ReadToEnd().Replace("&quot;", "\"");
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Response == null)
                    return DownloadPage(url, cancellationToken);

                if (((HttpWebResponse)ex.Response).StatusCode == HttpStatusCode.NotFound)
                    return string.Empty;

                return DownloadPage(url, cancellationToken);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public void WriteLog(LogLevel logLevel, string text)
        {
            Application.LogManager.WriteLog(logLevel, $"{Name} ::: {text}");
        }

        protected virtual void OnDownloadPage(HttpWebRequest httpWebRequest, Uri url, object userData)
        { }
    }
}
