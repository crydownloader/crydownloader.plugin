﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public interface IJsonWriter
    {
        void WriteStartObject();
        void WriteEndObject();
        void WriteStartArray();
        void WriteEndArray();
        void WriteStartConstructor(string name);
        void WriteEndConstructor();
        void WritePropertyName(string name);
        void WritePropertyName(string name, bool escape);
        void WriteEnd();
        void WriteNull();
        void WriteUndefined();
        void WriteRaw(string json);
        void WriteRawValue(string json);
        void WriteValue(string value);
        void WriteValue(int value);
        void WriteValue(uint value);
        void WriteValue(long value);
        void WriteValue(ulong value);
        void WriteValue(float value);
        void WriteValue(double value);
        void WriteValue(bool value);
        void WriteValue(short value);
        void WriteValue(ushort value);
        void WriteValue(char value);
        void WriteValue(byte value);
        void WriteValue(sbyte value);
        void WriteValue(decimal value);
        void WriteValue(DateTime value);
        void WriteValue(DateTimeOffset value);
        void WriteValue(Guid value);
        void WriteValue(TimeSpan value);
        void WriteValue(byte[] value);
        void WriteValue(Uri value);
        void WriteValue(object value);
        void WriteComment(string text);
        void WriteWhitespace(string ws);
    }
}
