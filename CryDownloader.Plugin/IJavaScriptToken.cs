﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptToken
    {
        JavaScriptTokens Type { get; set; }

        string Literal { get; set; }

        object Value { get; set; }

        int[] Range { get; set; }

        int? LineNumber { get; set; }

        int LineStart { get; set; }

        bool Octal { get; set; }

        IJavaScriptLocation Location { get; set; }

        int Precedence { get; set; }
    }
}
