﻿using System.Text.RegularExpressions;

namespace CryDownloader.Plugin
{
    public interface IJavaScriptRegExpInstance
    {
        Regex Value { get; }
        string Source { get; }
        string Flags { get; }
        bool Global { get; }
        bool IgnoreCase { get; }
        bool Multiline { get; }
        Match Match(string input, double start);
    }
}
