﻿namespace CryDownloader.Plugin
{
    public enum JavaScriptTypes
    {
        None,
        Undefined,
        Null,
        Boolean,
        String,
        Number,
        Object
    }
}
