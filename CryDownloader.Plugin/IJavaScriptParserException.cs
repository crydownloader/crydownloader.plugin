﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptParserException
    {
        int Column { get; }
        string Description { get; }
        int Index { get; }
        int LineNumber { get; }
    }
}
