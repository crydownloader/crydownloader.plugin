﻿using System;
using System.Threading;

namespace CryDownloader.Plugin
{
    public interface IShareHoster
    {
        bool CreateEntry(Uri url, IContainer container, CancellationToken? cancellationToken);
    }
}
