﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IContainer
    {
        string Name { get; }

        bool AddEntry(string filename, DownloadEntry entry, bool loadingEntry = true);
    }
}
