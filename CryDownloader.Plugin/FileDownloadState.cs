﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public enum FileDownloadState
    {
        None,
        Downloading,
        Resolving,
        Starting,
        Finished,
        Waiting,
        Cancelled,
        Paused,
        Unpack,
        Muxing,
        Failed,
    }
}
