﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public interface IJsonConverter
    {
        void WriteJson(IJsonWriter writer, object value, IJsonSerializer serializer);

        object ReadJson(IJsonReader reader, Type objectType, object existingValue, IJsonSerializer serializer);

        bool CanConvert(Type objectType);
    }
}
