﻿using System;

namespace CryDownloader.Plugin
{
    public sealed class FileStateChangedEventArgs : EventArgs
    {
        public FileStateIcon Icon { get; }
        public string StateDescription { get; }

        public FileStateChangedEventArgs(IFileState state)
        {
            Icon = state.Icon;
            StateDescription = state.Description;
        }
    }
}
