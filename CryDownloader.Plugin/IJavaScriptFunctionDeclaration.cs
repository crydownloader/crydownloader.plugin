﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IJavaScriptFunctionDeclaration : IJavaScriptStatement
    {
        IEnumerable<IJavaScriptExpression> Defaults { get; }
        IJavaScriptSyntaxNode Rest { get; }
        bool Generator { get; }
        bool Expression { get; }
        IJavaScriptIdentifier Id { get; }
        IEnumerable<IJavaScriptIdentifier> Parameters { get; }
        IJavaScriptStatement Body { get; }
        bool Strict { get; }
        ICollection<IJavaScriptVariableDeclaration> VariableDeclarations { get; }
        ICollection<IJavaScriptFunctionDeclaration> FunctionDeclarations { get; }
    }
}
