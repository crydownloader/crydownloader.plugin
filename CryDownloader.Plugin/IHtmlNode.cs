﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IHtmlNode
    {
        string InnerText { get; }
        string InnerHtml { get; }
        string Id { get; }
        bool HasClosingAttributes { get; }
        bool HasChildNodes { get; }
        IHtmlNode EndNode { get; }
        IHtmlNode FirstChild { get; }
        IHtmlNode LastChild { get; }
        IHtmlAttributeCollection ClosingAttributes { get; }
        bool Closed { get; }
        IHtmlNodeCollection ChildNodes { get; }
        bool HasAttributes { get; }
        int Line { get; }
        int InnerStartIndex { get; }
        IHtmlAttributeCollection Attributes { get; }
        int InnerLength { get; }
        int OuterLength { get; }
        string Name { get; }
        IHtmlNode NextSibling { get; }
        HtmlNodeType NodeType { get; }
        string OriginalName { get; }
        string OuterHtml { get; }
        IHtmlNode ParentNode { get; }
        IHtmlNode PreviousSibling { get; }
        int StreamPosition { get; }
        int LinePosition { get; }
        string XPath { get; }
        int Depth { get; }

        IEnumerable<IHtmlNode> Ancestors(string name);
        IEnumerable<IHtmlNode> Ancestors();
        IEnumerable<IHtmlNode> AncestorsAndSelf();
        IEnumerable<IHtmlNode> AncestorsAndSelf(string name);
        IEnumerable<IHtmlAttribute> ChildAttributes(string name);
        IEnumerable<IHtmlNode> Descendants();
        IEnumerable<IHtmlNode> Descendants(int level);
        IEnumerable<IHtmlNode> Descendants(string name);
        IEnumerable<IHtmlNode> DescendantsAndSelf(string name);
        IEnumerable<IHtmlNode> DescendantsAndSelf();
        IHtmlNode Element(string name);
        IEnumerable<IHtmlNode> Elements(string name);
        string GetAttributeValue(string name, string def);
        bool GetAttributeValue(string name, bool def);
        int GetAttributeValue(string name, int def);
        IEnumerable<string> GetClasses();
        string GetDirectInnerText();
        bool HasClass(string className);
        IHtmlNodeCollection SelectNodes(string xpath);
        IHtmlNode SelectSingleNode(string xpath);
    }
}
