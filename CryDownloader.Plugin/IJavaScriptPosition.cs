﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptPosition
    {
        int Line { get; }
        int Column { get; }
    }
}
