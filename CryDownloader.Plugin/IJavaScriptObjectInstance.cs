﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IJavaScriptObjectInstance
    {
        IJavaScriptObjectInstance Prototype { get; }
        bool Extensible { get; }
        string Class { get; }

        IEnumerable<KeyValuePair<string, IJavaScriptPropertyDescriptor>> GetOwnProperties();
        bool HasOwnProperty(string p);
        void RemoveOwnProperty(string p);
        IJavaScriptValue Get(string propertyName);
        IJavaScriptPropertyDescriptor GetOwnProperty(string propertyName);
        IJavaScriptPropertyDescriptor GetProperty(string propertyName);
        void Put(string propertyName, IJavaScriptValue value, bool throwOnError);
        bool CanPut(string propertyName);
        bool HasProperty(string propertyName);
        bool Delete(string propertyName, bool throwOnError);
        IJavaScriptValue DefaultValue(JavaScriptTypes hint);
        bool DefineOwnProperty(string propertyName, IJavaScriptPropertyDescriptor desc, bool throwOnError);
        void FastAddProperty(string name, IJavaScriptValue value, bool writable, bool enumerable, bool configurable);
        void FastSetProperty(string name, IJavaScriptPropertyDescriptor value);
    }
}
