﻿namespace CryDownloader.Plugin
{
    public enum FileStateIcon
    {
        None,
        Current,
        Error,
        Warning,
        Unpack,
        ExclamationMark,
        QuestionMark,
        CheckMark,
    }
}
