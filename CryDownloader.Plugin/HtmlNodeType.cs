﻿namespace CryDownloader.Plugin
{
    public enum HtmlNodeType
    {
        Document = 0,
        Element = 1,
        Comment = 2,
        Text = 3
    }
}
