﻿using System;
using System.IO;
using System.Net;

namespace CryDownloader.Plugin
{
    public abstract class DownloadEntry : MarshalByRefObject
    {
        public event EventHandler FileSizeChanged;
        public event EventHandler<DownloadProgressChangedEventArgs> DownloadProgressChanged;
        public event EventHandler<FileStateChangedEventArgs> FileStateChanged;
        public event EventHandler BeginDownload;
        public event EventHandler<DownloadCompleteEventArgs> CompleteDownload;

        public Hoster Hoster { get; }

        public string DownloadPath { get; set; }

        public Uri Url { get; protected set; }

        public FileSizeType FileSizeType { get; protected set; }

        public string FileName { get; set; }

        public bool IsDouble { get; set; }

        public IFileState FileState
        {
            get => m_fileState;
            set
            {
                m_fileState = value;

                try
                {
                    FileStateChanged?.Invoke(this, new FileStateChangedEventArgs(m_fileState));
                }
                catch (Exception)
                { }
            }
        }

        public long FileSize
        {
            get => m_fileSize;
            set
            {
                m_fileSize = value;
                FileSizeChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public IContainer Container { get; internal set; }

        public bool IsLoaded { get; private set; }

        private long m_fileSize;
        private IFileState m_fileState;

        public DownloadEntry(Hoster hoster)
        {
            Hoster = hoster;
            FileState = GetFileState(FileStateType.FileInfoNotAvail);
        }

        public void Load(object data)
        {
            OnLoad(data);
            IsLoaded = true;
        }

        public void Load(IContainer container)
        {
            if (container != null)
                Container = container;

            Load((object)null);
        }

        public virtual bool StartDownload(string path)
        {
            return DownloadFile(Url, path);
        }

        public abstract bool CheckStream(Stream stream);

        public bool DownloadFile(Uri url, string path)
        {
            bool hasError = false;
            bool retry = false;

            if (!Hoster.Application.Download(url, path, 
                () => BeginDownload?.Invoke(this, EventArgs.Empty), 
                (x) => {
                    CompleteDownload?.Invoke(this, x);

                    if (x.Error != null)
                    {
                        retry = x.Retry;
                        hasError = true;
                    }
                }, 
                (x) => DownloadProgressChanged?.Invoke(this, x)))
            {
                if (hasError && retry)
                {
                    Load(null); // refresh every information
                    return StartDownload(path);
                }

                return false;
            }

            return true;
        }

        protected abstract void OnLoad(object data);

        private long GetFileSize(Uri url, string method)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method;
            request.UserAgent = Hoster.UserAgent;

            OnWebRequestCreated(request, url);

            try
            {
                using (var response = request.GetResponse())
                {
                    return response.ContentLength;
                }
            }
            catch (WebException ex)
            {
                var response = ex.Response as HttpWebResponse;

                if (response != null)
                {
                    if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        if (method == "HEAD")
                        {
                            // file not found, maybe a HEAD problem?
                            // retry with GET
                            return GetFileSize(url, "GET");
                        }

                        return 0;
                    }
                    else if (response.StatusCode == HttpStatusCode.Forbidden)
                    {
                        if (method == "HEAD")
                        {
                            // file not found, maybe a HEAD problem?
                            // retry with GET
                            return GetFileSize(url, "GET");
                        }

                        return 0;
                    }
                }

                throw;
            }
        }

        protected long GetFileSize(Uri url)
        {
            return GetFileSize(url, "HEAD");
        }

        protected M3UPlaylist LoadM3UPlaylist(TextReader reader)
        {
            return new M3UPlaylist(reader);
        }

        protected M3UPlaylist LoadM3UPlaylist(string text)
        {
            using (var reader = new StringReader(text))
            {
                return new M3UPlaylist(reader);
            }
        }

        protected virtual void OnWebRequestCreated(HttpWebRequest request, Uri url)
        { }

        protected bool Muxing(string fileListPath, string outputPath)
        {
            MuxingArgs args = new MuxingArgs
            {
                FileListPath = fileListPath,
                OutputPath = outputPath
            };

            return Hoster.Application.Muxing(this, args, () => BeginDownload?.Invoke(this, EventArgs.Empty), (x) => CompleteDownload?.Invoke(this, x), (x) => DownloadProgressChanged?.Invoke(this, x));
        }

        protected bool Muxing(Uri videoUrl, Uri audioUrl, string outputPath)
        {
            MuxingArgs args = new MuxingArgs
            {
                VideoPath = videoUrl.ToString(),
                AudioPath = audioUrl.ToString(),
                OutputPath = outputPath
            };

            return Hoster.Application.Muxing(this, args, () => BeginDownload?.Invoke(this, EventArgs.Empty), (x) => CompleteDownload?.Invoke(this, x), (x) => DownloadProgressChanged?.Invoke(this, x));
        }

        protected T LoadJsonObject<T>(string json)
        {
            return Hoster.Application.LoadJsonObject<T>(json);
        }

        protected T LoadJsonObject<T>(TextReader reader)
        {
            return Hoster.Application.LoadJsonObject<T>(reader);
        }

        protected IHtmlDocument LoadHtmlDocument(string html)
        {
            return Hoster.Application.LoadHtmlDocument(html);
        }

        protected IFileState CreateFileState(FileStateIcon icon, string description)
        {
            return Hoster.Application.CreateFileState(icon, description);
        }

        protected IFileState GetFileState(FileStateType fileStateType)
        {
            return Hoster.Application.GetFileState(fileStateType);
        }
    }
}
