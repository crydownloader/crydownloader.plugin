﻿using System;

namespace CryDownloader.Plugin
{
    public interface IContainerCreator
    {
        Uri CurrentUrl { get; set; }
        IContainer CreateContainer(string name);
    }
}
