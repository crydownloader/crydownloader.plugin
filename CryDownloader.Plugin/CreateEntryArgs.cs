﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public class CreateEntryArgs
    {
        public Uri Url { get; internal set; }
        public IContainerCreator ContainerCreator { get; }
        public CancellationToken? CancellationToken { get; }
        public bool? IsHTML { get; internal set; }

        internal CreateEntryArgs(IContainerCreator containerCreator, CancellationToken? token)
        {
            ContainerCreator = containerCreator;
            CancellationToken = token;
        }
    }
}
