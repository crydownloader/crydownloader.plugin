﻿namespace CryDownloader.Plugin
{
    public interface IFileState
    {
        FileDownloadState DownloadState { get; }
        FileStateIcon Icon { get; }
        string Description { get; }
    }
}
