﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public abstract class CaptchaType : MarshalByRefObject, IPlugin
    {
        public abstract string Name { get; }
        public abstract bool NeedAction { get; }

        public IApplication Application { get; }

        public ILogManager LogManager => Application.LogManager;

        private Task m_checkCaptchaTask;
        private bool m_bIsRunning;
        private IManualResolveElement m_currentResolver;

        public CaptchaType(IApplication app)
        {
            Application = app;
        }

        public virtual void Destroy()
        {
            if (m_checkCaptchaTask != null)
            {
                m_bIsRunning = false;
                m_checkCaptchaTask.Wait();
            }
        }

        public abstract void Initialize();

        public abstract bool CanResolveCaptcha(IHtmlNode htmlNode);

        public abstract IEnumerable<IHtmlNode> GetCaptchaNodes(IHtmlDocument htmlDocument);

        public bool ResolveCaptcha(IHtmlNode htmlNode, IWebBrowser webBrowser,
            IManualResolveElement manualResolve, Action successCallback, object syncObject)
        {
            OnStartCheckCaptchaTask(webBrowser);

            if (!OnResolveCaptcha(htmlNode, webBrowser))
                return ShowManualResolve(manualResolve, successCallback, syncObject);

            if (WaitForCheckCaptchaTask())
                successCallback?.Invoke();

            return true;
        }

        protected abstract void OnStartCheckCaptchaTask(IWebBrowser webBrowser);

        protected abstract bool OnResolveCaptcha(IHtmlNode htmlNode, IWebBrowser webBrowser);

        protected void StartCheckCaptchaTask(Func<bool> callback)
        {
            m_checkCaptchaTask = new Task(() => ProcessCheckCaptcha(callback));

            m_bIsRunning = true;
            m_checkCaptchaTask.Start();
        }

        private bool ShowManualResolve(IManualResolveElement manualResolve, Action successCallback, object syncObject)
        {
            if (manualResolve == null)
            {
                m_bIsRunning = false;
                WaitForCheckCaptchaTask();
                return false;
            }

            lock (syncObject)
            {
                m_currentResolver = manualResolve;

                Application.Dispatcher.Invoke(() =>
                {
                    manualResolve.ShowDialog();
                });

                if (manualResolve.IsCancelled)
                {
                    m_bIsRunning = false;
                    WaitForCheckCaptchaTask();
                    return false;
                }
            }

            successCallback?.Invoke();
            return true;
        }

        private bool WaitForCheckCaptchaTask()
        {
            if (m_bIsRunning)
            {
                m_bIsRunning = false;

                if (m_checkCaptchaTask != null)
                    return m_checkCaptchaTask.Wait(new TimeSpan(0, 1, 0));
            }

            m_checkCaptchaTask = null;
            return true;
        }

        private void ProcessCheckCaptcha(Func<bool> callback)
        {
            while (m_bIsRunning)
            {
                if (callback())
                    break;
            }

            if (m_currentResolver != null)
                m_currentResolver.Close();

            m_bIsRunning = false;
        }
    }
}
