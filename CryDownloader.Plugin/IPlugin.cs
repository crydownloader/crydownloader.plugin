﻿namespace CryDownloader.Plugin
{
    public interface IPlugin
    {
        string Name { get; }

        void Initialize();
        void Destroy();

    }
}
