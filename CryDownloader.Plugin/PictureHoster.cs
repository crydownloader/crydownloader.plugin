﻿using System.Collections.Generic;
using System.Windows;

namespace CryDownloader.Plugin
{
    public abstract class PictureHoster : Hoster
    {
        public override FrameworkElement SettingControl => null;
        public bool NoThumb { get; set; }

        public PictureHoster(IApplication app)
            : base(app)
        {
            NoThumb = true;
        }

        public override Dictionary<string, string> GetSettings()
        {
            return new Dictionary<string, string>() { { "NoThumb", NoThumb ? "1" : "0" } };
        }

        public override void LoadSettings(Dictionary<string, string> properties)
        {
            NoThumb = properties["NoThumb"] == "1";
        }
    }
}
