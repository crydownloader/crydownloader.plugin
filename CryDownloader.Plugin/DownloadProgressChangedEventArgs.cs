﻿using System;

namespace CryDownloader.Plugin
{
    public class DownloadProgressChangedEventArgs : EventArgs
    {
        public long BytesReceived { get; }
        public long TotalBytesToReceive { get; }
        public double PercentComplete { get; }
        public double CurrentSpeed { get; }
        public DateTime? StartTime { get; }
        public TimeSpan? Duration { get; }

        public DownloadProgressChangedEventArgs(
            long bytesReceived,
            long totalBytesToReceive,
            double percentComplete,
            long currentSpeed,
            DateTime? startTime,
            TimeSpan? duration)
        {
            BytesReceived = bytesReceived;
            TotalBytesToReceive = totalBytesToReceive;
            PercentComplete = percentComplete;
            CurrentSpeed = currentSpeed;
            StartTime = startTime;
            Duration = duration;
        }

        public DownloadProgressChangedEventArgs(
            long bytesReceived,
            long totalBytesToReceive,
            double currentSpeed,
            DateTime? startTime,
            TimeSpan? duration)
        {
            BytesReceived = bytesReceived;
            TotalBytesToReceive = totalBytesToReceive;
            CurrentSpeed = currentSpeed;
            StartTime = startTime;
            Duration = duration;
            PercentComplete = Math.Round((double)bytesReceived / totalBytesToReceive * 100, 2, MidpointRounding.AwayFromZero);
        }

        public DownloadProgressChangedEventArgs(
            long bytesReceived,
            long totalBytesToReceive,
            double currentSpeed,
            DateTime? startTime)
        {
            BytesReceived = bytesReceived;
            TotalBytesToReceive = totalBytesToReceive;
            StartTime = startTime;
            CurrentSpeed = currentSpeed;
            PercentComplete = Math.Round((double)bytesReceived / totalBytesToReceive * 100, 2, MidpointRounding.AwayFromZero);

            if (CurrentSpeed != 0)
            {
                double dTimeRemaining = ((double)TotalBytesToReceive - BytesReceived) / CurrentSpeed;
                int timeRemaining = (int)dTimeRemaining;

                Duration = new TimeSpan(0, 0, 0, timeRemaining, (int)Math.Round((dTimeRemaining - timeRemaining) * 1000));
            }
        }

        public DownloadProgressChangedEventArgs(DownloadProgressChangedEventArgs old, long totalBytesToReceive)
        {
            BytesReceived = old.BytesReceived;
            TotalBytesToReceive = totalBytesToReceive;
            StartTime = old.StartTime;
            CurrentSpeed = old.CurrentSpeed;
            PercentComplete = (int)Math.Round((double)BytesReceived / totalBytesToReceive * 100, 2, MidpointRounding.AwayFromZero);
            Duration = old.Duration;
        }
    }
}
