﻿using System.Text;

namespace CryDownloader.Plugin
{
    public interface IHtmlDocument
    {
        string Text { get; }
        string ParsedText { get; }
        string Remainder { get; }
        Encoding DeclaredEncoding { get; }
        IHtmlNode DocumentNode { get; }
        Encoding Encoding { get; }
        int CheckSum { get; }
        int RemainderOffset { get; }
        Encoding StreamEncoding { get; }

        IHtmlNode GetElementbyId(string id);
    }
}
