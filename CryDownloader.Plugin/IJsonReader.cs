﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public interface IJsonReader
    {
        JsonToken TokenType { get; }
        object Value { get; }
        Type ValueType { get; }
        int Depth { get; }
        string Path { get; }
        CultureInfo Culture { get; set; }
        Task<bool> ReadAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task SkipAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<bool> ReadAsBooleanAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<byte[]> ReadAsBytesAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<DateTime> ReadAsDateTimeAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<DateTimeOffset> ReadAsDateTimeOffsetAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<decimal> ReadAsDecimalAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<double> ReadAsDoubleAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<int> ReadAsInt32Async(CancellationToken cancellationToken = default(CancellationToken));
        Task<string> ReadAsStringAsync(CancellationToken cancellationToken = default(CancellationToken));
        int ReadAsInt32();
        string ReadAsString();
        byte[] ReadAsBytes();
        double ReadAsDouble();
        bool ReadAsBoolean();
        decimal ReadAsDecimal();
        DateTime ReadAsDateTime();
        bool Read();
        void Skip();
    }
}
