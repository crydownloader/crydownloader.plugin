﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public class MuxingArgs
    {
        public string OutputPath { get; set; }
        public string FileListPath { get; set; }
        public string AudioPath { get; set; }
        public string VideoPath { get; set; }
    }
}
