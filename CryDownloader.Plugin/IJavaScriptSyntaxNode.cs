﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptSyntaxNode
    {
        JavaScriptSyntaxNodes Type { get; }
        IJavaScriptLocation Location { get; set; }
    }
}
