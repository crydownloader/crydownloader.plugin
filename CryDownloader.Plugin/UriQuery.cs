﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public sealed class UriQuery : IEnumerable<KeyValuePair<string, string>>
    {
        private Dictionary<string, string> m_queryElements;

        public string Query { get; }

        public int Count => m_queryElements.Count;

        public UriQuery(string strQuery)
        {
            Query = strQuery;
            m_queryElements = new Dictionary<string, string>();

            ParseQuery(strQuery);
        }

        public string this[string str]
        {
            get => m_queryElements[str];
        }

        public bool Contains(string str)
        {
            return m_queryElements.ContainsKey(str);
        }

        public bool TryGetElement(string str, out string value)
        {
            return m_queryElements.TryGetValue(str, out value);
        }

        public UriQuery(Uri uri)
        {
            Query = uri.Query;
            m_queryElements = new Dictionary<string, string>();

            ParseQuery(uri.Query);
        }

        private void ParseQuery(string strQuery)
        {
            if (strQuery.StartsWith("?"))
            {
                string[] splt = strQuery.Remove(0, 1).Split('&');

                foreach (var element in splt)
                {
                    string[] elementSplt = element.Split('=');

                    if (elementSplt.Length == 2)
                        m_queryElements.Add(elementSplt[0], elementSplt[1]);
                }
            }
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return m_queryElements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
