﻿using System;

namespace CryDownloader.Plugin
{
    public interface IJavaScriptDateInstance : IJavaScriptObjectInstance
    {
        double PrimitiveValue { get; }

        DateTime ToDateTime();
    }
}
