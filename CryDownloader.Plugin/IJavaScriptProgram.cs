﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IJavaScriptProgram
    {
        IJavaScriptValue Execute();
        IJavaScriptValue Invoke(string propertyName, object thisObject, params object[] arguments);
        IJavaScriptValue Invoke(string propertyName, params object[] arguments);
        IJavaScriptValue Invoke(IJavaScriptValue value, params object[] arguments);
        IJavaScriptValue Invoke(IJavaScriptValue value, object thisObject, params object[] arguments);
    }
}
