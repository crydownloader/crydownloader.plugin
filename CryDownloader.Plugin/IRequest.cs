﻿using System.Collections.Specialized;
using System.IO;

namespace CryDownloader.Plugin
{
    public interface IRequest
    {
        NameValueCollection Headers { get; set; }
        string Method { get; set; }
        string Url { get; set; }

        Stream GetRequestStream();
    }
}
