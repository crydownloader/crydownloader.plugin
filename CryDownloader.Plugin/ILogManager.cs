﻿using System;

namespace CryDownloader.Plugin
{
    public interface ILogManager
    {
        void WriteLog(LogLevel logLevel, string message);
        void WriteException(LogLevel logLevel, string message, Exception ex);
    }
}
