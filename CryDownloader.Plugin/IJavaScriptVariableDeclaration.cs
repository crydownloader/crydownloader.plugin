﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IJavaScriptVariableDeclaration : IJavaScriptStatement
    {
        IEnumerable<IJavaScriptVariableDeclarator> Declarations { get; }

        string Kind { get; }
    }
}
