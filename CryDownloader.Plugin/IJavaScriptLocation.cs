﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptLocation
    {
        IJavaScriptPosition Start { get; }
        IJavaScriptPosition End { get; }
        string Source { get; }
    }
}
