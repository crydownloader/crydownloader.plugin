﻿using System;
using System.Net;
using System.Windows;

namespace CryDownloader.Plugin
{
    public interface IWebBrowser : IDisposable
    {
        string UserAgent { get; }

        string GetElementValue(string id);
        void Navigate(Uri url);
        IHtmlNode GetElementById(string identifer);
        void SendClickEvent(IHtmlNode node);
        void SetValue(IHtmlNode node, string value);
        void LoadHTML(string html, string url);
        void LoadRequest(IRequest request);
        IRequest CreateRequest();
        Uri GetDownloadUrl();
        IHtmlDocument GetDocument();
        void Show(Visibility visibility);

        CookieContainer GetAllCookies();
        CookieContainer GetAllCookies(Uri url);
    }
}
