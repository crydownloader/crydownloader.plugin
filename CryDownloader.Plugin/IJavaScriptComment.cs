﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptComment
    {
        IJavaScriptLocation Location { get; set; }

        int[] Range { get; set; }

        string Type { get; }

        string Value { get; set; }
    }
}
