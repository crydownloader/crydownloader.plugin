﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IHtmlAttributeCollection : ICollection<IHtmlAttribute>
    {
        IHtmlAttribute this[string name] { get; }
        IHtmlAttribute this[int index] { get; }

        IEnumerable<IHtmlAttribute> AttributesWithName(string attributeName);

        bool Contains(string name);
        int IndexOf(IHtmlAttribute item);
    }
}
