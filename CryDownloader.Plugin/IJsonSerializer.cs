﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryDownloader.Plugin
{
    public interface IJsonSerializer
    {
        void Populate(TextReader reader, object target);
        void Populate(IJsonReader reader, object target);
        object Deserialize(IJsonReader reader);
        object Deserialize(TextReader reader, Type objectType);
        T Deserialize<T>(IJsonReader reader);
        object Deserialize(IJsonReader reader, Type objectType);
        void Serialize(TextWriter textWriter, object value);
        void Serialize(IJsonWriter jsonWriter, object value, Type objectType);
        void Serialize(TextWriter textWriter, object value, Type objectType);
        void Serialize(IJsonWriter jsonWriter, object value);
    }
}
