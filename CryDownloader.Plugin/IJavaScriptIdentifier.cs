﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptIdentifier : IJavaScriptExpression
    {
        string Name { get; }
    }
}
