﻿using System.Collections.Generic;

namespace CryDownloader.Plugin
{
    public interface IHtmlNodeCollection : ICollection<IHtmlNode>
    {
        IHtmlNode this[string nodeName] { get; }
        int this[IHtmlNode node] { get; }
        IHtmlNode this[int index] { get; }

        IEnumerable<IHtmlNode> Descendants(string name);
        IEnumerable<IHtmlNode> Descendants();
        IEnumerable<IHtmlNode> Elements(string name);
        IEnumerable<IHtmlNode> Elements();
        IHtmlNode FindFirst(string name);
        int GetNodeIndex(IHtmlNode node);
        int IndexOf(IHtmlNode item);
        IEnumerable<IHtmlNode> Nodes();
    }
}
