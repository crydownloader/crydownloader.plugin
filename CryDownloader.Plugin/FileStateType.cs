﻿namespace CryDownloader.Plugin
{
    public enum FileStateType
    {
        None,
        FileInfoNotAvail,
        FileInfoAvail,
        FileDeleted,
        FileExists,
        FileNotFound,
        Resolving,
    }
}
