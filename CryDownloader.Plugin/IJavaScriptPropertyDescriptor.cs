﻿namespace CryDownloader.Plugin
{
    public interface IJavaScriptPropertyDescriptor
    {
        IJavaScriptValue Get { get; }
        IJavaScriptValue Set { get; }
        bool? Enumerable { get; }
        bool? Writable { get; }
        bool? Configurable { get; }
        IJavaScriptValue Value { get; }
        bool IsAccessorDescriptor();
        bool IsDataDescriptor();
        bool IsGenericDescriptor();
    }
}
