﻿using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace CryDownloader.Plugin
{
    public interface IManualResolveElement
    {
        bool IsCancelled { get; }

        Dispatcher Dispatcher { get; }

        ManualResetEvent ConditionVariable { get; set; }

        bool IsOpen { get; }

        void ShowDialog();

        void Show(Visibility visibility);

        void Close();

        void HideActionBar();
    }
}
